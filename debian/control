Source: matplotlib2
Section: python
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Uploaders: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Build-Depends: debhelper (>= 7),
               dh-python,
               dvipng,
               graphviz,
               ipython,
               libfreetype6-dev,
               libpng-dev,
               locales-all,
               pkg-config,
               python-all-dbg (>= 2.4.4-6),
               python-all-dev (>= 2.3.5-7),
               python-backports.functools-lru-cache,
               python-cycler (>= 0.10.0),
               python-dateutil,
               python-colorspacious,
               python-cxx-dev,
               python-functools32,
               python-gi,
               python-kiwisolver,
               python-kiwisolver-dbg,
               python-mock,
               python-nose,
               python-numpy,
               python-numpy-dbg,
               python-numpydoc,
               python-pil,
               python-pkg-resources,
               python-pyparsing (>= 1.5.6),
               python-pytest,
               python-pyqt5 [!hurd-i386],
               python-scipy,
               python-setuptools,
               python-six (>= 1.4),
               python-sphinx (>= 1.0.7+dfsg),
               python-sphinx-gallery,
               python-subprocess32,
               python-tornado,
               python-tz,
               python-xlwt,
               texlive-fonts-recommended,
               texlive-latex-extra,
               texlive-latex-recommended,
               xauth,
               xvfb,
               zlib1g-dev
XS-Python-Version: all
Standards-Version: 4.3.0
Homepage: http://matplotlib.org/
Vcs-Git: https://salsa.debian.org/python-team/modules/matplotlib2.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/matplotlib2

Package: python-matplotlib
Architecture: any
Depends: python-backports.functools-lru-cache,
         python-dateutil,
         python-matplotlib2-data (>= ${source:Version}),
         python-pyparsing (>= 1.5.6),
         python-tz,
         libjs-jquery,
         ${misc:Depends},
         ${python:Depends},
         ${shlibs:Depends}
Recommends: python-pil,
            python-tk (>= 2.5.2-1.1)
Enhances: ipython
Suggests: dvipng,
          ffmpeg,
          gir1.2-gtk-3.0,
          ghostscript,
          inkscape,
          ipython (>= 0.6.3),
          librsvg2-common,
          python-cairocffi,
          python-configobj,
          python-excelerator,
          python-gi,
          python-gobject-2,
          python-matplotlib2-doc,
          python-nose,
          python-qt4,
          python-scipy,
          python-sip,
          python-tornado,
          python-traits (>= 2.0),
          python-wxgtk3.0,
          texlive-extra-utils,
          texlive-latex-extra,
          ttf-staypuft
Description: Python based plotting system in a style similar to Matlab
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.

Package: python-matplotlib2-data
Architecture: all
Depends: fonts-lyx, ${misc:Depends}, ttf-bitstream-vera
Description: Python based plotting system (data package)
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This package contains architecture independent data for python-matplotlib.

Package: python-matplotlib2-doc
Architecture: all
Section: doc
Depends: libjs-jquery, ${misc:Depends}
Description: Python based plotting system (documentation package)
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This package contains documentation for python-matplotlib.

Package: python-matplotlib-dbg
Architecture: any
Section: debug
Depends: python-all-dbg,
         python-matplotlib (= ${binary:Version}),
         python-kiwisolver-dbg,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Python based plotting system (debug extension)
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This package contains the debug extension for python-matplotlib.

